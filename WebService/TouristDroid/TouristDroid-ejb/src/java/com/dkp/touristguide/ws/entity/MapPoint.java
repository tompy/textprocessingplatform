/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkp.touristguide.ws.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Piotrex
 */
@Entity
@Table(name = "MapPoint")
@XmlRootElement(name = "MapPoint")
@NamedQueries({
    @NamedQuery(name = "MapPoint.findAll", query = "SELECT m FROM MapPoint m"),
    @NamedQuery(name = "MapPoint.findById", query = "SELECT m FROM MapPoint m WHERE m.id = :id"),
    @NamedQuery(name = "MapPoint.findByLatitude", query = "SELECT m FROM MapPoint m WHERE m.latitude = :latitude"),
    @NamedQuery(name = "MapPoint.findByLongitude", query = "SELECT m FROM MapPoint m WHERE m.longitude = :longitude"),
    @NamedQuery(name = "MapPoint.findByRadius", query = "SELECT m FROM MapPoint m WHERE m.radius = :radius"),
    @NamedQuery(name = "MapPoint.findByLabel", query = "SELECT m FROM MapPoint m WHERE m.label = :label"),
    @NamedQuery(name = "MapPoint.findByShortInfo", query = "SELECT m FROM MapPoint m WHERE m.shortInfo = :shortInfo")})
public class MapPoint implements Serializable {
    @Basic(optional =     false)
    @NotNull
    @Column(name = "LastUpdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @Size(max = 50)
    @Column(name = "Region")
    private String region;
    @Size(max = 100)
    @Column(name = "Address")
    private String address;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Longitude")
    private double longitude;
    @Column(name = "Radius")
    private Integer radius;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Label")
    private String label;
    @Size(max = 500)
    @Column(name = "ShortInfo")
    private String shortInfo;
    @Lob
    @Size(max = 65535)
    @Column(name = "FullInfo")
    private String fullInfo;
    public MapPoint() {
    }

    public MapPoint(Integer id) {
        this.id = id;
    }

    public MapPoint(Integer id, double latitude, double longitude, String label) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.label = label;
    }
    
    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getShortInfo() {
        return shortInfo;
    }

    public void setShortInfo(String shortInfo) {
        this.shortInfo = shortInfo;
    }

    public String getFullInfo() {
        return fullInfo;
    }

    public void setFullInfo(String fullInfo) {
        this.fullInfo = fullInfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MapPoint)) {
            return false;
        }
        MapPoint other = (MapPoint) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dkp.touristguide.ws.entity.MapPoint[ id=" + id + " ]";
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}