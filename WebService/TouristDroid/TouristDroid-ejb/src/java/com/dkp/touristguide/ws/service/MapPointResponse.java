/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkp.touristguide.ws.service;

import com.dkp.touristguide.ws.entity.MapPoint;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Piotrex
 */
@XmlRootElement(name = "MapPointResponse")
public class MapPointResponse implements Serializable {
    
    private List<MapPoint> mapPoint;
    private String currentDate;
    
    public MapPointResponse() {
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public List<MapPoint> getMapPoint() {
        return mapPoint;
    }

    public void setMapPoint(List<MapPoint> mapPoint) {
        this.mapPoint = mapPoint;
    }
}