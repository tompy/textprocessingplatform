package com.dkp.touristguide.map;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import com.dkp.touristguide.database.TouristPoint;

public class TouristPointItem extends OverlayItem {

	private TouristPoint touristPoint;

	public TouristPointItem(TouristPoint point) {
		this(point.getLabel(), point.getFullInfo(), new GeoPoint(
				point.getLatitude(), point.getLongitude()));
		this.touristPoint = point;
	}

	private TouristPointItem(String aTitle, String aDescription,
			GeoPoint aGeoPoint) {
		super(aTitle, aDescription, aGeoPoint);
		// TODO Auto-generated constructor stub
	}

	public TouristPoint getTouristPoint() {
		return touristPoint;
	}

	public void setTouristPoint(TouristPoint touristPoint) {
		this.touristPoint = touristPoint;
	}

}