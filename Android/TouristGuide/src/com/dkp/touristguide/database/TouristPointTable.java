package com.dkp.touristguide.database;

public class TouristPointTable {

	private static final String TABLE_POINTS = "TouristPoints";
	
	public static final String COLUMN_ID 		= "ID";
	public static final String COLUMN_LAT 	= "Latitude";
	public static final String COLUMN_LON 	= "Longitude";
	public static final String COLUMN_RAD 	= "Radius";
	public static final String COLUMN_NAME 	= "Label";
	public static final String COLUMN_SINFO 	= "ShortInfo";
	public static final String COLUMN_FINFO 	= "FullInfo";
	public static final String COLUMN_TYPE 	= "Type";
	public static final String COLUMN_VARSION = "Version";
	
	private static final String CREATE_TABLE = "";
}
