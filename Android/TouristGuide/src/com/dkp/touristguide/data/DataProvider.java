package com.dkp.touristguide.data;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.osmdroid.util.BoundingBoxE6;

import android.content.Context;
import android.widget.Toast;

import com.dkp.touristguide.database.DatabaseHandler;
import com.dkp.touristguide.database.TouristPoint;
import com.dkp.touristguide.webservice.TouristPointServiceResult;

public class DataProvider {
	private Context context;
	private DatabaseHandler dbHandler;

	public DataProvider(Context context) {
		this.context = context;
		dbHandler = new DatabaseHandler(context);
	}

	public String updatePoints(String url) {
		return updatePoints(url, null, null);
	}

	public String updatePoints(String url, String date, String region) {
		String regionsArray[] = new String[] { region };

		WebServiceConnectionTask task;
		TouristPointServiceResult taskResult;
		
		try {
			if (dbHandler.countAllPoints() < 1)
				date = null;
			
			task = new WebServiceConnectionTask();
			task.execute(addStringArrays(new String[] {
					WebServiceConnectionTask.UPDATE, url, date }, regionsArray));
			taskResult = task.get();
			if (taskResult == null)
				throw new Exception("Błąd aktualizacji");

			for (TouristPoint point : taskResult.getPointsList())
				dbHandler.addTouristPoint(point);
			
			Toast.makeText(context, "Zaktualizowano", Toast.LENGTH_LONG).show();
			
			return taskResult.getUpdateDate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(context, "Błąd aktualizacji", Toast.LENGTH_LONG)
					.show();
		}
		return null;
	}

	public List<TouristPoint> getAllTouristPoints() {
		return dbHandler.getAllTouristPoints();
	}

	public List<TouristPoint> getAllTouristPointsInsideBox(BoundingBoxE6 box) {
		return dbHandler.getAllInsideBox(box);
	}

	public String[] addStringArrays(String[] a, String[] b) {
		String[] c = new String[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);

		return c;
	}
}
