package com.dkp.touristguide.data;

import org.ksoap2.serialization.SoapObject;

import com.dkp.touristguide.webservice.WebServiceConnector;

import android.os.AsyncTask;

public class CheckUpdateTask extends AsyncTask<Void, Void, Boolean> {

	private WebServiceConnector wsConnector;
	
	@Override
	protected Boolean doInBackground(Void... params) {
		wsConnector = new WebServiceConnector();
		SoapObject response = wsConnector.getAllPointsResponse();
		return consumeResponse(response);
	}

	private Boolean consumeResponse(SoapObject response) {
		// TODO Auto-generated method stub
		return false;
	}

}
