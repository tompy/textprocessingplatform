var markers;

function init() {
      
    map = new OpenLayers.Map("map", {
        controls: [
            new OpenLayers.Control.Navigation(),
            //new OpenLayers.Control.PanZoomBar(),
            new OpenLayers.Control.ScaleLine(),
            new OpenLayers.Control.KeyboardDefaults()
        ],
        eventListeners: {
            "moveend": mapEvent,
            "zoomend": mapEvent
        },
        projection: new OpenLayers.Projection("EPSG:900913")
    });

    var pzb = new OpenLayers.Control.PanZoomBar({'position': new OpenLayers.Pixel(10, 40) });
    map.addControl(pzb);

    map.addLayer(new OpenLayers.Layer.OSM());

    var latS = 51.871661;
    var lonS = 15.612666;
    var zoom = 7;

    var startPoint = new OpenLayers.LonLat(lonS, latS)
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            new OpenLayers.Projection("EPSG:900913") // to Spherical Mercator Projection
          );


    if (sessionStorage.lat)
    {
        if (sessionStorage.lon)
        {
            latS = Number(sessionStorage.lat);
            lonS = Number(sessionStorage.lon);
            var startPoint = new OpenLayers.LonLat(lonS, latS);
        }
    }

    if (sessionStorage.zoom) zoom = Number(sessionStorage.zoom);

    markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);
    
    addMarkers();

    map.setCenter(startPoint, zoom);
     
    // Dla widoku View zczytujemy położenie punktu.
    setMapCoord();
    

    map.events.register("click", map, function (e) {
        var latlon = map.getLonLatFromViewPortPx(e.xy).transform(new OpenLayers.Projection("EPSG:900913"), 
                                                                 new OpenLayers.Projection("EPSG:4326"));
        var lat = latlon.lat;
        var lon = latlon.lon;  

        if (OpenLayers.Util.getElement("latitude") != null) {
            OpenLayers.Util.getElement("latitude").value = lat;            
        }

        if (OpenLayers.Util.getElement("longitude") != null) {
            OpenLayers.Util.getElement("longitude").value = lon;            
        }
    });

    map.events.register("mousemove", map, function (e) {
        var latlon = map.getLonLatFromViewPortPx(e.xy).transform(new OpenLayers.Projection("EPSG:900913"), 
                                                                 new OpenLayers.Projection("EPSG:4326"));
        var lat = latlon.lat;
        var lon = latlon.lon;  
        OpenLayers.Util.getElement("xyLat").innerHTML = lat;
        OpenLayers.Util.getElement("xyLon").innerHTML = lon;
    }); 
}
                
function mapEvent(event) {
    var lat = map.getCenter().lat;
    var lon = map.getCenter().lon;  

    sessionStorage.lat = lat;
    sessionStorage.lon = lon; 
    sessionStorage.zoom = map.getZoom();
}

function setMapCoord(){    
    var otLat = document.getElementById('otLat');
    var otLon = document.getElementById('otLon');

    if (otLat != null) {
        var coord = new OpenLayers.LonLat(Number(otLon.innerText), Number(otLat.innerText))
              .transform(
                new OpenLayers.Projection("EPSG:4326"),
                new OpenLayers.Projection("EPSG:900913")
              );

        map.setCenter(coord, 17);
    }    
}

function addMarker(lat, lon, popupContentHTML) {    
    var point = new OpenLayers.LonLat(Number(lat), Number(lon)).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection(map.projection));    
    popupClass = OpenLayers.Popup.FramedCloud;
    var feature = new OpenLayers.Feature(markers, point); 
    feature.closeBox = true;
    feature.popupClass = popupClass;
    feature.data.popupContentHTML = popupContentHTML;
    feature.data.overflow = (true) ? "auto" : "hidden";

    var marker = feature.createMarker();

    var markerClick = function (evt) {
        if (this.popup == null) {
            this.popup = this.createPopup(this.closeBox);
            map.addPopup(this.popup);
            this.popup.show();
        } else {
            this.popup.toggle();
        }
        currentPopup = this.popup;
        OpenLayers.Event.stop(evt);
    };
    marker.events.register("mousedown", feature, markerClick);

    markers.addMarker(marker);
}