package com.dkp.touristguide.webservice;

import java.util.List;

import com.dkp.touristguide.database.TouristPoint;

public class TouristPointServiceResult {
	private List<TouristPoint> pointsList;
	private String updateDate;
	private boolean updateAvaliable;
	
	public TouristPointServiceResult() {
		this.updateAvaliable = false;
		this.updateDate = null;
		this.pointsList = null;
	}

	public boolean isUpdateAvaliable() {
		return updateAvaliable;
	}

	public void setUpdateAvaliable(boolean updateAvaliable) {
		this.updateAvaliable = updateAvaliable;
	}
	
	public void setUpdateResult(List<TouristPoint> pointsList, String updateDate) {
		this.pointsList = pointsList;
		this.updateDate = updateDate;
		this.updateAvaliable = true;
	}

	public List<TouristPoint> getPointsList() {
		return pointsList;
	}

	public String getUpdateDate() {
		return updateDate;
	}
	
}
