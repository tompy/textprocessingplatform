package com.dkp.touristguide.voice_interface;

import java.util.ArrayList;
import java.util.List;

public class VoiceCommandReslover {

	private static VoiceCommandReslover conversationReslover;
	private List<VoiceCommand> voiceCommandsList;
	private VoiceCommand activeCommand;

	private VoiceCommandReslover() {
		voiceCommandsList = new ArrayList<VoiceCommand>();
		activeCommand = null;
	}

	public static VoiceCommandReslover getVoiceCommandReslover() {
		if (conversationReslover == null)
			conversationReslover = new VoiceCommandReslover();
		return conversationReslover;
	}

	public boolean resloveVoiceCommand(List<String> voiceInputList) {
		for (String voiceInput : voiceInputList) {
			for (VoiceCommand voiceCommand : voiceCommandsList) {
				if (voiceCommand.checkCommand(voiceInput)) {
					voiceCommand.activeVoiceCommand(voiceInput);
					activeCommand = voiceCommand;
					return true;
				}
			}
		}
		
		return false;
	}

	public void registerCommand(VoiceCommand voiceCommand) {
		voiceCommandsList.add(voiceCommand);
	}

	public VoiceCommand getActiveCommand() {
		return activeCommand;
	}

}
