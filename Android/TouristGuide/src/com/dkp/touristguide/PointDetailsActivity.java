package com.dkp.touristguide;

import org.osmdroid.util.GeoPoint;

import com.dkp.touristguide.database.TouristPointTable;
import com.dkp.touristguide.database.TouristPointsProvider;

import android.os.Bundle;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PointDetailsActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {

	private String pointName;
	private TextView tvName, tvCoords, tvShortInfo, tvFullInfo;
	private Button btClose;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_details);
        
        tvName = (TextView) findViewById(R.id.tvName);
        tvCoords = (TextView) findViewById(R.id.tvCoords);
        tvShortInfo = (TextView) findViewById(R.id.tvShortInfo);
        tvFullInfo = (TextView) findViewById(R.id.tvFullInfo);
        
        btClose =(Button) findViewById(R.id.btClose);
        btClose.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				stop();
			}
		});
        
        Bundle extras = getIntent().getExtras();
        pointName = extras.getString(TouristPointsProvider.POINT_MIME_TYPE);
        getLoaderManager().restartLoader(1, null, this);
    }
    
    private void stop() {
		setResult(RESULT_OK);
		finish();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_point_details, menu);
        return true;
    }
    
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader cursorLoader = new CursorLoader(getApplicationContext(),
				TouristPointsProvider.POINTNAME_URI, null, null,
				new String[] { pointName }, null);
		return cursorLoader;
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		String name = "", shortInfo = "", fullInfo = "";
		double lat = 54.371574, lon = 18.612451;
		if (cursor.moveToFirst()) {
			name = cursor.getString(cursor
					.getColumnIndex(TouristPointTable.COLUMN_NAME));
			lat = cursor.getDouble(cursor
					.getColumnIndex(TouristPointTable.COLUMN_LAT));
			lon = cursor.getDouble(cursor
					.getColumnIndex(TouristPointTable.COLUMN_LON));
			shortInfo = cursor.getString(cursor
					.getColumnIndex(TouristPointTable.COLUMN_SINFO));
			fullInfo = cursor.getString(cursor
					.getColumnIndex(TouristPointTable.COLUMN_FINFO));
		}

		tvName.setText("Nazwa: " + name);
		tvCoords.setText("Współrzędne: " + lat + ", " + lon);
		tvShortInfo.setText("Krótki opis: " + shortInfo);
		tvFullInfo.setText("Opis: " + fullInfo);
	}

	public void onLoaderReset(Loader<Cursor> loader) {
		return;
	}

	@Override
	public void onBackPressed() {
		stop();
	}
	
	
}
