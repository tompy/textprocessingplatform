package com.dkp.touristguide;

import java.security.Provider;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;

import com.dkp.touristguide.data.DataProvider;
import com.dkp.touristguide.database.TouristPoint;
import com.dkp.touristguide.database.TouristPointTable;
import com.dkp.touristguide.database.TouristPointsProvider;
import com.dkp.touristguide.voice_interface.SearchCommand;
import com.dkp.touristguide.voice_interface.VoiceCommandReslover;

public class LaunchActivity extends Activity implements RecognitionListener,
		OnInitListener, LocationListener, MapListener,
		LoaderManager.LoaderCallbacks<Cursor> {
	private MapView mvMain;
	private MapController mMapController;
	private SpeechRecognizer speechRecognizer;
	private Button btSpeak;
	private TextToSpeech myTTS;
	private VoiceCommandReslover commandReslover = VoiceCommandReslover
			.getVoiceCommandReslover();
	private List<TouristPoint> tpLists;
	private ItemizedOverlay<OverlayItem> mMyLocationOverlay;
	private ResourceProxy mResourceProxy;
	private DataProvider dataProvider;
	private Context context;
	private LocationManager locationManager;

	private static final String LOG_NAME = "LaunchActivity";
	private static final int POINT_DETAILS_REQUEST = 18;
	private int MY_DATA_CHECK_CODE = 0;

	private String pointName;
	private TouristPoint currentPoint;
	private boolean navigate;
	private boolean gpsProvider, networkProvider;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launch);

		context = getApplicationContext();
		mResourceProxy = new DefaultResourceProxyImpl(context);

		dataProvider = new DataProvider(context);

		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		gpsProvider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		networkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		
		if (gpsProvider) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, this);
			Log.d("LaunchActivity", "Started GPS provider");
		} else if (networkProvider) { 
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 0, 0, this);
			Log.d("LaunchActivity", "Started network provider");
		} else
			Log.d("LaunchActivity", "No location provider");

		initMapView();
		initSpeechRecognizer();
		initSpeechButton();
		initVoiceCommands();
		initTouristPoints();
		initPointsOverlay();

		checkTTS();
	}

	private void initMapView() {
		mvMain = (MapView) findViewById(R.id.mvMain);
		mvMain.setTileSource(TileSourceFactory.MAPNIK);
		mvMain.setBuiltInZoomControls(true);
		mvMain.setMultiTouchControls(true);
		mMapController = mvMain.getController();
		mMapController.setZoom(18);
		GeoPoint gPt = new GeoPoint(54.371574, 18.612451);
		mMapController.setCenter(gPt);
		mvMain.setMapListener(this);
	}

	private void initSpeechRecognizer() {
		speechRecognizer = SpeechRecognizer
				.createSpeechRecognizer(getApplicationContext());
		speechRecognizer.setRecognitionListener(this);
	}

	private void initSpeechButton() {
		btSpeak = (Button) findViewById(R.id.btSpeak);
		btSpeak.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent listenIntent = new Intent(
						RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
				listenIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
						getClass().getPackage().getName());
				listenIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
						"pl-PL");
				listenIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);

				speechRecognizer.startListening(listenIntent);
				Log.d(LOG_NAME, "New Recognizer Intent started");

				btSpeak.setEnabled(false);
			}
		});
	}
	
	private void initVoiceCommands() {
		SearchCommand sc = new SearchCommand(this);
	}

	private void initTouristPoints() {
		SharedPreferences mySharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		boolean checkUpdatesOnStart = mySharedPreferences.getBoolean("synchronization", true);
		
		if (checkUpdatesOnStart)
			updateLocalDatabase();
		
		tpLists = dataProvider.getAllTouristPoints();
	}

	private void updateLocalDatabase() {
		SharedPreferences mySharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String url = mySharedPreferences
				.getString("web_service_addres",
						"http://176.122.224.123:8080/MapPointServiceService/MapPointService");
		
		Log.d("LaunchActivity", url);
		
		String lastUpdateDate = null;
		Set<String> regions = mySharedPreferences.getStringSet("regions", new HashSet<String>());
		
		Log.d("LaunchActivity", regions.toString());

		if (isNetworkAvailable()) {
			for (String region : regions)	{
				lastUpdateDate = mySharedPreferences
						.getString(region+"_update_date", null);
				
				String newUpdateDate = dataProvider.updatePoints(url, lastUpdateDate, region);
				
				if (newUpdateDate != null) {
					mySharedPreferences.edit().putString(region+"_update_date", newUpdateDate).commit();
					Log.d("LaunchActivity", region+"_update_date: " + newUpdateDate);
				}
			}
		}
	}

	private void initPointsOverlay() {
		ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();
		for (TouristPoint tp : tpLists)
			items.add(new OverlayItem(tp.getLabel(), tp.getFullInfo(),
					new GeoPoint(tp.getLatitude(), tp.getLongitude())));

		mMyLocationOverlay = new ItemizedIconOverlay<OverlayItem>(items,
				new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {

					public boolean onItemLongPress(int arg0, OverlayItem arg1) {
						return false;
					}

					public boolean onItemSingleTapUp(int arg0, OverlayItem item) {
//						Toast.makeText(context, item.mDescription.toString(),
//								Toast.LENGTH_LONG).show();
						myTTS.speak(item.mDescription.toString(),
								TextToSpeech.QUEUE_FLUSH, null);
						pointTpped(item.mTitle.toString());
						return false;
					}

				}, mResourceProxy);
		mvMain.getOverlays().add(this.mMyLocationOverlay);
		mvMain.invalidate();
	}

	protected void pointTpped(String pointName) {
		Intent detailsIntent = new Intent("android.intent.action.POINT_DETAILS");
		detailsIntent
				.putExtra(TouristPointsProvider.POINT_MIME_TYPE, pointName);
		startActivityForResult(detailsIntent, POINT_DETAILS_REQUEST);
	}

	private void checkTTS() {
		Intent checkTTSIntent = new Intent();
		checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MY_DATA_CHECK_CODE) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				myTTS = new TextToSpeech(this, this);
			} else {
				Intent installTTSIntent = new Intent();
				installTTSIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installTTSIntent);
			}
		} else if (requestCode == SearchCommand.SEARCH_ACTIVITY) {
			if (resultCode == 1) {
				pointName = data.getExtras().getString(
						TouristPointsProvider.POINT_MIME_TYPE);
				getLoaderManager().restartLoader(1, null, this);
			}
		} if (requestCode == POINT_DETAILS_REQUEST) {
			if (resultCode == RESULT_OK) {
				myTTS.stop();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_launch, menu);
		return true;
	}

	public void onBeginningOfSpeech() {
		// btSpeak.setEnabled(false);
	}

	public void onBufferReceived(byte[] arg0) {
		// TODO Auto-generated method stub

	}

	public void onEndOfSpeech() {
		btSpeak.setEnabled(true);
		speechRecognizer.stopListening();
	}

	public void onError(int arg0) {
		Toast.makeText(getApplicationContext(), "Network Error",
				Toast.LENGTH_SHORT).show();
	}

	public void onEvent(int arg0, Bundle arg1) {
		return;
	}

	public void onPartialResults(Bundle bundle) {
		return;
	}

	public void onReadyForSpeech(Bundle bundle) {
		Toast.makeText(getApplicationContext(), "SPEAK", Toast.LENGTH_SHORT)
				.show();
	}

	public void onResults(Bundle bundle) {
		ArrayList<String> list = bundle
				.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		if (list != null && !list.isEmpty()) {
			if (!commandReslover.resloveVoiceCommand(list)) {
				Toast.makeText(getApplicationContext(),
						"Błąd - Niepoprawne polecenie: " + list.get(0),
						Toast.LENGTH_SHORT).show();
			}
			// else {
			// Toast.makeText(context, list.get(0), Toast.LENGTH_SHORT).show();
			// }
		} else {
			Toast.makeText(getApplicationContext(),
					"Błąd - Brak polecenia głosowego", Toast.LENGTH_SHORT)
					.show();
		}
	}

	public void onRmsChanged(float arg0) {
		// TODO Auto-generated method stub

	}

	public void onInit(int initStatus) {
		if (initStatus == TextToSpeech.SUCCESS) {
			myTTS.setLanguage(new Locale("pl_PL"));
		} else if (initStatus == TextToSpeech.ERROR) {
			Toast.makeText(this, "Błąd TTS", Toast.LENGTH_LONG).show();
		}
	}

	public void onLocationChanged(Location location) {
		
		GeoPoint currentLocation = new GeoPoint(location.getLatitude(), location.getLongitude());
		SharedPreferences mySharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		int maxRange = Integer.parseInt(mySharedPreferences.getString("notification_range", "30"));
		String notification_mode = mySharedPreferences.getString("notification_type", "SHORT_INFO");
		
		if (navigate) {
			Log.d("LaunchActivity", "Location changed to " + location.getLatitude() + " " + location.getLongitude());
			
			if (currentPoint != null)
				Log.d("LaunchActivity", "Distance to old point :" +currentLocation.distanceTo(new GeoPoint(currentPoint.getLatitude(), currentPoint.getLongitude())));
			
			if (currentPoint != null && currentLocation.distanceTo(new GeoPoint(currentPoint.getLatitude(), currentPoint.getLongitude())) > maxRange) {
				currentPoint = null;
				Log.d("LaunchActivity", "old point reset");
			}
						
			mMapController.setCenter(currentLocation);
			mMapController.setZoom(18);
			
			TouristPoint closestPoint = null;
			GeoPoint prevPoint = null;
			
			for(TouristPoint tp : tpLists) {
				GeoPoint tpLocation = new GeoPoint(tp.getLatitude(), tp.getLongitude());
				
				if (currentPoint == null || !currentPoint.getLabel().equals(tp.getLabel())) {
					if (closestPoint == null || (currentLocation.distanceTo(tpLocation) < (currentLocation.distanceTo(prevPoint)))) {
						closestPoint = tp;
						prevPoint = tpLocation;
					}
				}
			}
			
			if (closestPoint != null && prevPoint != null) {
				int range = currentLocation.distanceTo(prevPoint);
				
				if(range < maxRange) {
					currentPoint = closestPoint;
					Log.d("LaunchActivity", "Notify");
					
					if ("SHORT_INFO".equals(notification_mode))
						notifyShortInfo(closestPoint, range);
					else if ("FULL_INFO".equals(notification_mode))
						notifyLongInfo(closestPoint);
				}				
			}
		}
	}

	private void notifyLongInfo(TouristPoint closestPoint) {
		Toast.makeText(context, closestPoint.getFullInfo(),
				Toast.LENGTH_LONG).show();
		myTTS.speak(closestPoint.getFullInfo(),
				TextToSpeech.QUEUE_FLUSH, null);
	}

	private void notifyShortInfo(TouristPoint closestPoint, int range) {
		String info;
		info = "W Promieniu" + range + "metrów znajduje się: " + closestPoint.getLabel();
		Toast.makeText(context, closestPoint.getLabel(),
				Toast.LENGTH_LONG).show();
		myTTS.speak(info,
				TextToSpeech.QUEUE_ADD, null);
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public boolean onScroll(ScrollEvent scroll) {
		reloadTouristPoints();
		return false;
	}

	public boolean onZoom(ZoomEvent zoom) {
		reloadTouristPoints();
		return false;
	}

	private void reloadTouristPoints() {
		mvMain.getOverlays().clear();
		if (mvMain.getZoomLevel() > 10) {
			tpLists = dataProvider.getAllTouristPointsInsideBox(mvMain
					.getBoundingBox());
			initPointsOverlay();
		}
	}

	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader cursorLoader = new CursorLoader(context,
				TouristPointsProvider.POINTNAME_URI, null, null,
				new String[] { pointName }, null);
		return cursorLoader;
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		double lat = 54.371574, lon = 18.612451;
		if (cursor.moveToFirst()) {
			lat = cursor.getDouble(cursor
					.getColumnIndex(TouristPointTable.COLUMN_LAT));
			lon = cursor.getDouble(cursor
					.getColumnIndex(TouristPointTable.COLUMN_LON));
		}

		// TODO init tp and show ttsDialog

		GeoPoint gPt = new GeoPoint(lat, lon);
		mMapController.setCenter(gPt);
	}

	public void onLoaderReset(Loader<Cursor> loader) {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.freeroam:
			navigate = false;
			break;

		case R.id.navigate:
			currentPoint = null;
			navigate = true;
			break;
			
		case R.id.update:
			updateLocalDatabase();
			break;

		case R.id.menu_settings:
			openSettings();
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void openSettings() {
		Intent settingsIntent = new Intent(getApplicationContext(),
				SettingsActivity.class);
		startActivity(settingsIntent);
	}

}
