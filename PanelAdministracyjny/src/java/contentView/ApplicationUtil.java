package contentView;

import com.dkp.touristguide.ws.service.MapPointService;
import com.dkp.touristguide.ws.service.MapPointServiceService;
import database.entities.MapPoint;
import database.manager.MapPointFacade;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "applicationUtil")
@SessionScoped
public class ApplicationUtil {

    @EJB
    private MapPointFacade mapPointFacade;
    
    private MapPointServiceService service;    
    private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
 	
    public ApplicationUtil() {
    }
       
    public void changeLanguage(String lang) {
        locale = new Locale(lang);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }
    
    public List<String[]> getAllPoints() {        
        List<MapPoint> listMp = mapPointFacade.findAll();
        List<String[]> list = new ArrayList<String[]>();
        
        for (MapPoint point : listMp) {            
            String[] tab = new String[3];
            tab[0] = Double.toString(point.getLongitude());  
            tab[1] = Double.toString(point.getLatitude());
            tab[2] = "<center>" + point.getLabel() + "<br></center>";
            list.add(tab);            
        }
        
        return list;
    }
    
    public String tabTooltip(String text) {
        String updatedText = "\n" + "     " + text + "     " + "\n ";                
        return updatedText;
    }
    
    
    public Double roundDouble(String number) {  
        double aDouble = Double.parseDouble(number);
        DecimalFormat df = new DecimalFormat("#.######");   
        String sNumber = df.format(aDouble).replace(',', '.');
        return Double.parseDouble(sNumber);
    }
    
    public String convertDate(Date date) {  
        DateFormat df = new SimpleDateFormat("MM.dd.yy - HH:mm");
        return df.format(date).replace("\n", "");
    }
       
    public String reverseGeocode(String lon, String lat) {        
        service = new MapPointServiceService();
        MapPointService port = service.getMapPointServicePort();
        String address = port.reverseGeocode(lat, lon);
        return address;
    }

    public MapPointServiceService getService() {
        return service;
    }

    public void setService(MapPointServiceService service) {
        this.service = service;
    }    
    
    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
        
    public DropList[] dropList;
    
    public static class DropList{
        public String label;
        public String value;

        public DropList(String label, String value){
                this.label = label;
                this.value = value;
        }

        public String getLabel(){
                return label;
        }

        public String getValue(){
                return value;
        }
    }
        
    public DropList[] getListValue(String item) { 
        
        dropList = new DropList[3];
        dropList[0] = new DropList("Pomorze", "Pomorze");
        dropList[1] = new DropList("Polska centralna", "Polska centralna");
        dropList[2] = new DropList("Śląsk", "Śląsk");
        
        for (int i = 0; i < dropList.length; i++) {
            if (dropList[i].value.equals(item)) {
                if (i != 0) {
                    DropList o = dropList[0];
                    dropList[0] = dropList[i];
                    dropList[i] = o;
                }
                break;
            }
        }
        
        return dropList; 
    }
}
