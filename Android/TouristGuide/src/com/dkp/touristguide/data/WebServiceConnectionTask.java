package com.dkp.touristguide.data;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.dkp.touristguide.database.TouristPoint;
import com.dkp.touristguide.webservice.TouristPointServiceResult;
import com.dkp.touristguide.webservice.WebServiceConnector;

import android.os.AsyncTask;
import android.util.Log;

public class WebServiceConnectionTask extends AsyncTask<String, Void, TouristPointServiceResult> {

	private static final String SHORT_INFO_ELEMENT = "shortInfo";
	private static final String LONGITUDE_ELEMENT = "longitude";
	private static final String LATITUDE_ELEMENT = "latitude";
	private static final String MAPPOINT_COMPLEX_TYPE = "mapPoint";
	private static final String FULL_INFO_ELEMENT = "fullInfo";
	private static final String LABEL_ELEMENT = "label";
	
	private static final String RESPONSE_START_TAG = "return";
	private static final String DATE_PROPERTY = "currentDate";
	
	public static final String UPDATE = "update";
	public static final String CHECK_FOR_UPDATES = "check";

	private WebServiceConnector wsConnector;
	private String mode;
	private String url;
	private String date;
	private List<String> regionsList;
	
	private List<TouristPoint> pointsList;
	private String updateDate;
	private boolean hasUpdates;

	@Override
	protected TouristPointServiceResult doInBackground(String... params) {
		TouristPointServiceResult result = new TouristPointServiceResult();		
		resloveParams(params);
		
		if (mode == null || url == null)
			return null;
		
		try {
			wsConnector = new WebServiceConnector(url);
			SoapObject response;
			
			Log.d("WebServiceConnectionTask", "mode: "+mode +" date: "+date + " region_list: " +regionsList.toString());
			
			if (UPDATE.equals(mode)) {
				response = wsConnector.getUpdatedPointsList(date, regionsList);
				consumeUpdateResponse(response);
				result.setUpdateResult(pointsList, updateDate);
			} else if (CHECK_FOR_UPDATES.equals(mode)) {
				response = wsConnector.checkForPointsUpdate(date, regionsList);
				consumeCheckForUpdateResponse(response);
				result.setUpdateAvaliable(hasUpdates);
			}
			
		} catch(Exception e) {
			return null;
		}
		
		return result;
	}

	private void consumeUpdateResponse(SoapObject response) throws Exception {
		consumeAllPointsResponse(response);
	}
	
	private void consumeCheckForUpdateResponse(SoapObject response) throws Exception {
		if (response.hasProperty(RESPONSE_START_TAG))
			hasUpdates = Boolean.parseBoolean(response.getPropertyAsString(RESPONSE_START_TAG));
		else 
			hasUpdates = false;	
	}

	private void resloveParams(String... params) {
		if (params.length < 3)
			return;
		
		mode = params[0];
		url = params[1];
		date = params[2];
		regionsList = new ArrayList<String>();
		
		for (int i = 3; i < params.length; i++)
			regionsList.add(params[i]);
	}

	private List<TouristPoint> consumeAllPointsResponse(SoapObject response) throws Exception{	
		pointsList = new ArrayList<TouristPoint>();
		
		if (response.hasProperty(RESPONSE_START_TAG)) {
			SoapObject result = (SoapObject) response.getProperty(RESPONSE_START_TAG);
			
			if (result.hasProperty(DATE_PROPERTY)) {
				updateDate = result.getPropertyAsString(0);
			}
			
			if (result.getPropertyCount() < 2)
				Log.d("TouristPointServiceResult", "Nie ma żadnych nowych punktów");
			
			for (int i = 1; i < result.getPropertyCount(); i++) {
				if (result.hasProperty(MAPPOINT_COMPLEX_TYPE)) {
					SoapObject mapPoint = (SoapObject) result.getProperty(i);
					
					String label = mapPoint.getPropertyAsString(LABEL_ELEMENT);
					String latitude = mapPoint.getPropertyAsString(LATITUDE_ELEMENT);
					String longitude = mapPoint.getPropertyAsString(LONGITUDE_ELEMENT);
					String shortInfo = mapPoint.getPropertyAsString(SHORT_INFO_ELEMENT);
					String	fullInfo = mapPoint.getPropertyAsString(FULL_INFO_ELEMENT);
					
					TouristPoint tp = new TouristPoint(Double.parseDouble(latitude), Double.parseDouble(longitude), label);
					tp.setShortInfo(shortInfo);
					tp.setFullInfo(fullInfo);
					
					pointsList.add(tp);
					
					Log.d("WEB SERVICE RESPONSE", updateDate + ": " + fullInfo + label + latitude + longitude + shortInfo);
				}
			}
		}
		
		return pointsList;
	}
}
