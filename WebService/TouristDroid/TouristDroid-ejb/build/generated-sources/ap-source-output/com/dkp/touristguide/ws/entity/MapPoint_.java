package com.dkp.touristguide.ws.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2012-12-02T20:52:19")
@StaticMetamodel(MapPoint.class)
public class MapPoint_ { 

    public static volatile SingularAttribute<MapPoint, String> region;
    public static volatile SingularAttribute<MapPoint, Integer> id;
    public static volatile SingularAttribute<MapPoint, String> address;
    public static volatile SingularAttribute<MapPoint, Date> lastUpdate;
    public static volatile SingularAttribute<MapPoint, String> shortInfo;
    public static volatile SingularAttribute<MapPoint, Double> longitude;
    public static volatile SingularAttribute<MapPoint, Integer> radius;
    public static volatile SingularAttribute<MapPoint, String> label;
    public static volatile SingularAttribute<MapPoint, Double> latitude;
    public static volatile SingularAttribute<MapPoint, String> fullInfo;

}