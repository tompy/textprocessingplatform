package contentView;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@ManagedBean(name = "loginHelper")
@SessionScoped
public class LoginHelper {

    private String username;
    private String password;
    
    public LoginHelper() {
    }
    
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        try {
            request.login(username, password);
            return "/mapPoint/List?faces-redirect-true";
        } catch (ServletException e) {
            //context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bad Login", null));
            return null;
        }
    }
    
    public String logout() {
        ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();        
        return "/login?faces-redirect=true";
    }

    public boolean isAdmin(){
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN");
    }
    
    public String getRole(){
        String role = "null";
        
        if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN")) {
            role = "ADMIN";
        }
        if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("USER")) {
            role = "USER";
        }
        return role;
    }
    
    public void checkStatus() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler) fc.getApplication().getNavigationHandler(); 
        
        String s = fc.getViewRoot().getViewId();
        
        if (FacesContext.getCurrentInstance().getExternalContext().getRemoteUser() != null) {            
            nav.performNavigation("/mapPoint/List?faces-redirect-true");
        }
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword() {
        return this.password;
    }
}
