/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkp.touristguide.ws.service;

import com.dkp.touristguide.ws.entity.MapPoint;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author Piotrex
 */
@Stateless
@WebService
public class MapPointService {
    
    @PersistenceContext(unitName = "MapPointPU")
    private EntityManager em;
    
    @WebMethod
    public MapPointResponse findNearestPoints(@WebParam(name="longtitude") String longtitude, @WebParam(name="latitude") String latitude, @WebParam(name="pointCount") String pointCount) {
        List<MapPoint> results = em.createNamedQuery("MapPoint.findAll").getResultList();
        MapPointResponse response = new MapPointResponse();
        response.setMapPoint(results);
        GregorianCalendar calendar = new GregorianCalendar();
        response.setCurrentDate(calendar.getTime().toString());
        return response;
    }
    
    @WebMethod
    public boolean checkUpdate(@WebParam(name="lastUpdateDate") String lastUpdateDate, @WebParam(name="region") String region) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastUpdateDate);
        } catch (ParseException ex) { }
        List<MapPoint> results;
        if ((region != null) && (!region.equals(""))) {
             results = em.createQuery("SELECT m FROM MapPoint m WHERE m.lastUpdate > :lastUpdate and m.region like :region").setParameter("lastUpdate", date).setParameter("region", region).getResultList();
        } else {
            results = em.createQuery("SELECT m FROM MapPoint m WHERE m.lastUpdate > :lastUpdate").setParameter("lastUpdate", date).getResultList();
        }
        if (results.size() > 0)
            return true;
        return false;
    }
    
    @WebMethod
    public MapPointResponse update(@WebParam(name="lastUpdateDate") String lastUpdateDate, @WebParam(name="region") String region) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastUpdateDate);
        } catch (ParseException ex) { }
        List<MapPoint> results = null;
        if ((lastUpdateDate == null) || (lastUpdateDate.equals(""))) {
            if ((region != null) && (!region.equals(""))) {
                results = em.createQuery("SELECT m FROM MapPoint m WHERE m.region like :region").setParameter("region", region).getResultList();
            } else {
                results = em.createQuery("SELECT m FROM MapPoint m").getResultList();
            }
        } else if ((region != null) && (!region.equals(""))) {
            results = em.createQuery("SELECT m FROM MapPoint m WHERE m.lastUpdate > :lastUpdate and m.region like :region").setParameter("lastUpdate", date).setParameter("region", region).getResultList();
        }
        MapPointResponse response = new MapPointResponse();
        response.setMapPoint(results);
        GregorianCalendar calendar = new GregorianCalendar();
        response.setCurrentDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime()));
        return response;
    }
    
    @WebMethod
    public String reverseGeocode(@WebParam(name="longtitude") String longtitude, @WebParam(name="latitude") String latitude) {
        ReverseGeocoding rg = new ReverseGeocoding();
        String result = rg.reverseGeocode(longtitude, latitude);
        //String address = rg.getDataFromXML(result, "//formatted_address/text()");
        String province = rg.getDataFromXML(result, "//address_component[type='administrative_area_level_1']/long_name/text()");
        String country = rg.getDataFromXML(result, "//address_component[type='country']/long_name/text()");
        String city = rg.getDataFromXML(result, "//address_component[type='administrative_area_level_3']/long_name/text()");
        String street = rg.getDataFromXML(result, "//address_component[type='route']/long_name/text()");
        String streetNumber = rg.getDataFromXML(result, "//address_component[type='street_number']/long_name/text()");
        String address = country + ", " + province + ", " + city + ", " + street + " " + streetNumber;
        return address;
    }
    
    private double calculateDistance(double longtitude1, double latitude1, double longtitude2, double latitude2) {
        double latDiff = 999999;
        double longDiff = 999999;
        if (latitude1 >= latitude2) {
            latDiff = latitude1 - latitude2;
        } else {
            latDiff = latitude2 - latitude1;
        }
        if (longtitude1 >= longtitude2) {
            longDiff = longtitude1 - longtitude2;
        } else {
            longDiff = longtitude2 - longtitude1;
        }
        double distance = (latDiff*latDiff) + (longDiff*longDiff);
        distance = Math.sqrt(distance);
        return distance;
    }
}