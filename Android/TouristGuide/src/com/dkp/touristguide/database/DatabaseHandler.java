package com.dkp.touristguide.database;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.util.BoundingBoxE6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "TouristGeoPoints";
	private static final int DATABASE_VERSION = 15;

	private static final String TABLE_POINTS = "TouristPoints";
	private static final String TABLE_POINTS_FTS = "TouristPointsFTS";

	private static final String KEY_ID 		= "_id";
	private static final String KEY_LAT 		= "Latitude";
	private static final String KEY_LON 		= "Longitude";
	private static final String KEY_RAD 		= "Radius";
	private static final String KEY_NAME 		= "Label";
	private static final String KEY_SINFO 	= "ShortInfo";
	private static final String KEY_FINFO 	= "FullInfo";
	private static final String KEY_TYPE 		= "Type";
	private static final String KEY_VARSION 	= "Version";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("CREATE VIRTUAL TABLE  ").append(TABLE_POINTS_FTS).append(" USING fts3 ( ")
				.append(KEY_ID).append(" INTEGER PRIMARY KEY ,")
				.append(KEY_LAT).append(" DOUBLE NOT NULL,").append(KEY_LON)
				.append(" DOUBLE NOT NULL,").append(KEY_RAD)
				.append(" INTEGER,").append(KEY_NAME).append(" TEXT UNIQUE,")
				.append(KEY_SINFO).append(" TEXT,").append(KEY_FINFO)
				.append(" TEXT )");

		database.execSQL(queryBuilder.toString());
		
		queryBuilder = new StringBuilder();
		queryBuilder.append("CREATE TABLE  ").append(TABLE_POINTS).append(" ( ")
				.append(KEY_ID).append(" INTEGER PRIMARY KEY ,")
				.append(KEY_LAT).append(" DOUBLE NOT NULL,").append(KEY_LON)
				.append(" DOUBLE NOT NULL,").append(KEY_RAD)
				.append(" INTEGER,").append(KEY_NAME).append(" TEXT UNIQUE,")
				.append(KEY_SINFO).append(" TEXT,").append(KEY_FINFO)
				.append(" TEXT )");

		database.execSQL(queryBuilder.toString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POINTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POINTS_FTS);
		this.onCreate(db);
	}

	public boolean addTouristPoint(TouristPoint touristPoint) {
		SQLiteDatabase db = this.getWritableDatabase();
		long err = 0;

		ContentValues values = new ContentValues();
		values.put(KEY_LAT, touristPoint.getLatitude());
		values.put(KEY_LON, touristPoint.getLongitude());
		values.put(KEY_RAD, touristPoint.getRadius());
		values.put(KEY_NAME, touristPoint.getLabel());
		values.put(KEY_SINFO, touristPoint.getShortInfo());
		values.put(KEY_FINFO, touristPoint.getFullInfo());

		// err = db.insert(TABLE_POINTS, null, values);
		err = db.insertWithOnConflict(TABLE_POINTS_FTS, null, values,
				SQLiteDatabase.CONFLICT_REPLACE);

		if (err == -1)
			return false;
		
		err = db.insertWithOnConflict(TABLE_POINTS, null, values,
				SQLiteDatabase.CONFLICT_REPLACE);
		
		if (err == -1)
			return false;
		
		db.close();
		
		return true;
	}

	public TouristPoint getTouristPoint(String pointName) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_POINTS, new String[] { KEY_LAT, KEY_LON,
				KEY_NAME, KEY_SINFO, KEY_FINFO }, KEY_NAME + "=?",
				new String[] { pointName }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		TouristPoint touristPoint = new TouristPoint(Double.parseDouble(cursor
				.getString(0)), Double.parseDouble(cursor.getString(1)),
				cursor.getString(2));
		touristPoint.setShortInfo(cursor.getString(3));
		touristPoint.setFullInfo(cursor.getString(4));
		
		return touristPoint;
	}

	public List<TouristPoint> getAllTouristPoints() {
		String selectQuery = "SELECT  * FROM " + TABLE_POINTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);


		return pointsListFromCursor(cursor);
	}

	public List<TouristPoint> getAllInsideBox(BoundingBoxE6 box) {
		Cursor cursor = getPointsInsideBoxCursor(box);	    	 
		return pointsListFromCursor(cursor);
	}
	
	public List<TouristPoint> searchByText(String textToSearch) {	
		Cursor cursor = searchPointsCursor(textToSearch);
		return pointsListFromCursor(cursor);
	}

	private Cursor getPointsInsideBoxCursor(BoundingBoxE6 box) {
		String latMin, latMax, lonMin, lonMax;
		latMin = Double.toString(box.getLatSouthE6() / 1.0E6);
		latMax = Double.toString(box.getLatNorthE6() / 1.0E6);
		lonMin = Double.toString(box.getLonWestE6() / 1.0E6);
		lonMax = Double.toString(box.getLonEastE6() / 1.0E6);
		String selectionArgs[] = new String[]{latMin, latMax, lonMin, lonMax};
		
		return getPointsBetweenCordsCursor(selectionArgs);
	}

	public Cursor getPointsBetweenCordsCursor(String[] selectionArgs) {
		StringBuilder selectQueryBuilder = new StringBuilder();
		selectQueryBuilder.append("SELECT  * FROM ").append(TABLE_POINTS)
			.append(" WHERE (").append(KEY_LAT).append(" BETWEEN ? AND ? ) AND (").append(KEY_LON).append(" BETWEEN ? AND ? )");
		
		Log.d("DatabaseHandler", selectQueryBuilder.toString());
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQueryBuilder.toString(), selectionArgs);
		return cursor;
	}
	
	public Cursor getPointByName(String name) {
		StringBuilder selectQueryBuilder = new StringBuilder();
		selectQueryBuilder.append("SELECT  * FROM ").append(TABLE_POINTS)
			.append(" WHERE (").append(KEY_NAME).append(" =? )");
		
		Log.d("DatabaseHandler", selectQueryBuilder.toString());
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQueryBuilder.toString(), new String[]{name});
		return cursor;
	}
	
	private List<TouristPoint> pointsListFromCursor(Cursor cursor) {
		List<TouristPoint> turistPointsList = new ArrayList<TouristPoint>();
		if (cursor.moveToFirst()) {
	        do {
	        	TouristPoint touristPoint = new TouristPoint(Double.parseDouble(cursor.getString(1)),
	    				Double.parseDouble(cursor.getString(2)), cursor.getString(4));
	        	touristPoint.setShortInfo(cursor.getString(5));
	    		touristPoint.setFullInfo(cursor.getString(6));

	            turistPointsList.add(touristPoint);
	        } while (cursor.moveToNext());
	    }
		return turistPointsList;
	}
	
	public Cursor searchPointsCursor(String textToSearch) {
		StringBuilder selectQueryBuilder = new StringBuilder();
		String selectQuery = "SELECT DISTINCT  * FROM " + TABLE_POINTS_FTS + " WHERE " + TABLE_POINTS_FTS + " MATCH ?";
		    
		SQLiteDatabase db = this.getWritableDatabase();

		return db.rawQuery(selectQuery, new String[]{textToSearch+"*"});
	}
	
	public int countAllPoints() {
		int all = 0;
		StringBuilder selectQueryBuilder = new StringBuilder();
		selectQueryBuilder.append("SELECT  COUNT(*) FROM ").append(TABLE_POINTS);
		
		Log.d("DatabaseHandler", selectQueryBuilder.toString());
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQueryBuilder.toString(), null);
	    if (cursor.moveToFirst()) {
	    	all = cursor.getInt(0);
	    }
	    
		return all;
	}

	public void deleteTouristPoint(String pointName) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_POINTS, KEY_NAME + " = ?", new String[] { pointName });
		db.delete(TABLE_POINTS_FTS, KEY_NAME + " = ?", new String[] { pointName });
		db.close();
	}
	
}
