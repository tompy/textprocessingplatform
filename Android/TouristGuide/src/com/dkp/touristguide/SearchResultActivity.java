package com.dkp.touristguide;

import java.util.List;

import com.dkp.touristguide.database.TouristPoint;
import com.dkp.touristguide.database.TouristPointTable;
import com.dkp.touristguide.database.TouristPointsProvider;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class SearchResultActivity extends ListActivity implements
		LoaderManager.LoaderCallbacks<Cursor> {

	private SimpleCursorAdapter adapter;
	private String textToSearch;

	public static final String TEXT_TO_SEARCH = "TEXT_TO_SEARCH";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_result);

		Bundle extras = getIntent().getExtras();
		textToSearch = extras.getString(TEXT_TO_SEARCH);

		String columns[] = new String[] { TouristPointTable.COLUMN_NAME };
		int into[] = new int[] { R.id.pointName };

		adapter = new SimpleCursorAdapter(this, R.layout.search_result_entry,
				null, columns, into, 0);

		getLoaderManager().initLoader(0, null, this);

		setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.search_result, menu);
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent data = new Intent();
		
		Cursor resultSet = (Cursor) adapter.getItem(position);
		String name = resultSet.getString(resultSet.getColumnIndex(TouristPointTable.COLUMN_NAME));
		Log.d("SearchResultActivity.onListItemClick", name);
		
		data.putExtra(TouristPointsProvider.POINT_MIME_TYPE, name);
		setResult(1, data);
		finish();
	}

	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader cursorLoader = new CursorLoader(getApplicationContext(),
				TouristPointsProvider.CONTENT_URI, null, null,
				new String[] { textToSearch }, null);
		return cursorLoader;
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		adapter.swapCursor(data);

	}

	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}

}
