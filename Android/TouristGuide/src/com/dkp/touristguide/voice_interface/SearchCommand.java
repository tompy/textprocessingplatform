package com.dkp.touristguide.voice_interface;

import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.widget.Toast;

import com.dkp.touristguide.SearchResultActivity;
import com.dkp.touristguide.database.DatabaseHandler;
import com.dkp.touristguide.database.TouristPoint;

public class SearchCommand extends VoiceCommand {
	private static final String SEARCH_COMMAND_TEXT = "szukaj";
	// private static SearchCommand searchCommandInstance;
	private DatabaseHandler dbHandler;
	private Context context;
	private Activity callingActivity;

	public static final int SEARCH_ACTIVITY = 101;

	public SearchCommand(Activity callingActivity) {
		super(SEARCH_COMMAND_TEXT);
		VoiceCommandReslover.getVoiceCommandReslover().registerCommand(this);
		this.context = callingActivity.getApplicationContext();
		this.callingActivity = callingActivity;
		dbHandler = new DatabaseHandler(context);
	}

	// public static SearchCommand getSearchCommand() {
	// if (searchCommandInstance == null)
	// searchCommandInstance = new SearchCommand(SEARCH_COMMAND_TEXT, null);
	// return searchCommandInstance;
	// }

	@Override
	public void activeVoiceCommand(String commandText) {
		String textToSearch = commandText.substring(SEARCH_COMMAND_TEXT
				.length() + 1);
		// List<TouristPoint> searchResultPoints =
		// dbHandler.searchByText(textToSearch);
		//
		// StringBuilder searchResult = new StringBuilder();
		// for(TouristPoint tp : searchResultPoints)
		// searchResult.append(tp.getLabel() + "\n");

		// Toast.makeText(context, searchResult.toString(),
		// Toast.LENGTH_LONG).show();

		Intent searchIntent = new Intent("android.intent.action.SEARCH_RESULT");
		searchIntent
				.putExtra(SearchResultActivity.TEXT_TO_SEARCH, textToSearch);

		callingActivity.startActivityForResult(searchIntent, SEARCH_ACTIVITY);
	}

}
