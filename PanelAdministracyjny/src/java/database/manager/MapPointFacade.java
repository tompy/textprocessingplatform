package database.manager;

import database.entities.MapPoint;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class MapPointFacade extends AbstractFacade<MapPoint> {
    @PersistenceContext(unitName = "PanelAdminstracyjnyPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MapPointFacade() {
        super(MapPoint.class);
    }
    
}
