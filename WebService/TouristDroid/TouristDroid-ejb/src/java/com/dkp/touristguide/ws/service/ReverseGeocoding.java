/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkp.touristguide.ws.service;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.*;
import javax.xml.xpath.*;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.InputSource;
/**
 *
 * @author Piotrex
 */
public class ReverseGeocoding {

    private static final String URL = "http://maps.googleapis.com/maps/api/geocode/xml"; 
    
    public String reverseGeocode(String longtitude, String latitude) {
        ByteArrayOutputStream output = null;
        try {
            URL url = new URL(URL + "?latlng=" + URLEncoder.encode(latitude, "UTF-8")+ "," + URLEncoder.encode(longtitude, "UTF-8") + "&sensor=false");
            URLConnection conn = url.openConnection();
            output = new ByteArrayOutputStream(1024);
            IOUtils.copy(conn.getInputStream(), output);
            output.close();
        } catch (Exception ex) { }
        String result = null;
        try {
            result = output.toString("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ReverseGeocoding.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String getDataFromXML(String xml, String expression) {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = docFactory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));
            XPathExpression expr = xpath.compile(expression);
            return (String) expr.evaluate(doc, XPathConstants.STRING);
        } catch (Exception ex) {
            return ex.toString();
        }
    }
}