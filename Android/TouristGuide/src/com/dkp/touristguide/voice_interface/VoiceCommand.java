package com.dkp.touristguide.voice_interface;

import android.util.Log;

public abstract class VoiceCommand {
	protected final String COMMAND_TEXT;
	
	protected VoiceCommand(String commandText) {
		COMMAND_TEXT = commandText;
	}
	
	public String getVoiceCommandText() {
		return COMMAND_TEXT;
	}

	public boolean checkCommand(String voiceInput) {
		Log.d("VoiceCommand", voiceInput + "/" + COMMAND_TEXT);
		if (voiceInput.toLowerCase().startsWith(COMMAND_TEXT))
			return true;
		return false;
	}
	
	public abstract void activeVoiceCommand(String commandText);
}
