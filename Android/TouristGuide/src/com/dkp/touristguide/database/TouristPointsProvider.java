package com.dkp.touristguide.database;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class TouristPointsProvider extends ContentProvider {

	private DatabaseHandler dbHandler;

	public static String AUTHORITY = "com.dkp.touristguide.database.TouristPointsProvider";
	private static final String BASE_URI = "points";
	private static final String POINTNAME = "name";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_URI);
	public static final Uri POINTNAME_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_URI + "/" + POINTNAME);

	public static final String POINTS_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/points";
	public static final String POINT_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/point";
	public static final String DEFINITION_MIME_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/vnd.dkp.touristguide.database.touristguide";

	private static final int SEARCH_POINTS = 0;
	private static final int INSIDE_BOX_POINTS = 1;
	private static final int GET_POINT = 2;
	private static final int GET_POINT_BY_NAME = 3;
	private static final int INSERT_POINT = 4;
	private static final int UPDATE_POINT = 5;
	private static final int DELETE_POINT = 6;
	private static final int SEARCH_SUGGEST = 7;
	private static final int REFRESH_SHORTCUT = 8;

	private static final UriMatcher sURIMatcher = buildUriMatcher();

	@Override
	public boolean onCreate() {
		dbHandler = new DatabaseHandler(getContext());
		return false;
	}

	private static UriMatcher buildUriMatcher() {
		UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(AUTHORITY, BASE_URI, SEARCH_POINTS);
		matcher.addURI(AUTHORITY, BASE_URI + "/#", GET_POINT);
		matcher.addURI(AUTHORITY, BASE_URI + "/" + POINTNAME, GET_POINT_BY_NAME);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY,
				SEARCH_SUGGEST);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*",
				SEARCH_SUGGEST);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT,
				REFRESH_SHORTCUT);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT
				+ "/*", REFRESH_SHORTCUT);
		return matcher;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		switch (sURIMatcher.match(uri)) {
		case SEARCH_SUGGEST:
			if (selectionArgs == null) {
				throw new IllegalArgumentException(
						"selectionArgs must be provided for the Uri: " + uri);
			}
			return getSuggestions(selectionArgs[0]);
		case SEARCH_POINTS:
			if (selectionArgs == null) {
				throw new IllegalArgumentException(
						"selectionArgs must be provided for the Uri: " + uri);
			}
			return searchPoints(selectionArgs[0]);
		case GET_POINT:
			return getPoint(uri);
		case GET_POINT_BY_NAME:
			return getPointByName(selectionArgs[0]);
		case REFRESH_SHORTCUT:
			return refreshShortcut(uri);
		default:
			throw new IllegalArgumentException("Unknown Uri: " + uri);
		}
	}

	private Cursor getSuggestions(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	private Cursor searchPoints(String textToSearch) {
		return dbHandler.searchPointsCursor(textToSearch);
	}

	private Cursor getPoint(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private Cursor getPointByName(String name) {
		return dbHandler.getPointByName(name);
	}

	private Cursor refreshShortcut(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getType(Uri uri) {
		switch (sURIMatcher.match(uri)) {
		case SEARCH_POINTS:
			return POINTS_MIME_TYPE;
		case GET_POINT_BY_NAME:
			return POINT_MIME_TYPE;
		case GET_POINT:
			return POINT_MIME_TYPE;
		case SEARCH_SUGGEST:
			return SearchManager.SUGGEST_MIME_TYPE;
		case REFRESH_SHORTCUT:
			return SearchManager.SHORTCUT_MIME_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URL " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

}
