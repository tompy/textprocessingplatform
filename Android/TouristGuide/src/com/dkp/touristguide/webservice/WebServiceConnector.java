package com.dkp.touristguide.webservice;

import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;

public class WebServiceConnector {

	private static final String SERVICE_URL = "http://176.122.224.123:8080/MapPointServiceService/MapPointService";
	private static final String SERVICE_NAMESPACE = "http://service.ws.touristguide.dkp.com/";
	
	private static final String METHOD_GET_POINTS = "findNearestPoints";
	private static final String CHECK_FOR_UPDATE = "checkUpdate";
	private static final String UPDATE = "update";
	
	private static final String SOAP_ACTION_GET_POINTS = "http://service.ws.touristguide.dkp.com/MapPointService/findNearestPointsRequest";

	private static final String GET_POINTS_INPUT_ARG1 = "longtitude";
	private static final String GET_POINTS_INPUT_ARG2 = "latitude";
	private static final String GET_POINTS_INPUT_ARG3 = "pointCount";
	
	private static final String INPUT_LAST_UPDATE = "lastUpdateDate";
	private static final String INPUT_REGION = "region";
	private String url;

	public WebServiceConnector() {
		this.url = SERVICE_URL;
	}
	
	public WebServiceConnector(String url) {
		this.url = url;
	}

	public SoapObject checkForPointsUpdate(String lastUpdatedDate, List<String> regions) {
		SoapObject requset = prepareDefultRequest(CHECK_FOR_UPDATE, lastUpdatedDate, regions);
		return callWebService(requset, SOAP_ACTION_GET_POINTS);
	}

	public SoapObject getUpdatedPointsList(String lastUpdatedDate, List<String> regions) {
		Log.d("WebServiceConnector", "method: " + UPDATE + " last_update: " + lastUpdatedDate + " regions: " + regions.toString());
		SoapObject requset = prepareDefultRequest(UPDATE, lastUpdatedDate, regions);
		return callWebService(requset, SOAP_ACTION_GET_POINTS);
	}

	public SoapObject getAllPointsResponse() {
		SoapObject requset = prepareGetAllPointsRequest(METHOD_GET_POINTS);
		return callWebService(requset, SOAP_ACTION_GET_POINTS);
	}

	private SoapObject prepareDefultRequest(String method, String date, List<String> regions) {
		SoapObject request = prepareRequest(method);

		request.addProperty(INPUT_LAST_UPDATE, date);
		for (String region : regions) {
			request.addProperty(INPUT_REGION, region);
		}

		return request;
	}
	
	private SoapObject prepareGetAllPointsRequest(String method) {
		SoapObject request = prepareRequest(method);

		request.addProperty(GET_POINTS_INPUT_ARG1, "54.371574");
		request.addProperty(GET_POINTS_INPUT_ARG2, "18.612451");
		request.addProperty(GET_POINTS_INPUT_ARG3, "10");

		return request;
	}

	private SoapObject prepareRequest(String method) {
		return new SoapObject(SERVICE_NAMESPACE, method);
	}

	private SoapObject callWebService(SoapObject request, String soapAction) {
		SoapSerializationEnvelope envelope = prepareSoapEnvelope(request);
		return sendRequest(soapAction, envelope);
	}

	private SoapSerializationEnvelope prepareSoapEnvelope(SoapObject request) {
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		// envelope.dotNet = true;
		return envelope;
	}

	private SoapObject sendRequest(String soapAction,
			SoapSerializationEnvelope envelope) {
		HttpTransportSE httpTransportSE = new HttpTransportSE(url);
		
		Log.d("WebServiceConnector", url);
		
		try {
			httpTransportSE.call(soapAction, envelope);
			return (SoapObject) envelope.bodyIn;
		} catch (Exception e) {
			Log.e(this.getClass().getName(), e.getMessage());
		}
		return null;
	}
}
